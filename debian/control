Source: gmpc
Section: sound
Priority: optional
Maintainer: mpd maintainers <pkg-mpd-maintainers@lists.alioth.debian.org>
Uploaders: Antoine Beaupré <anarcat@debian.org>,
           Etienne Millon <me@emillon.org>,
           Simon McVittie <smcv@debian.org>
Build-Depends: debhelper-compat (= 13),
               gob2,
               intltool,
               libglib2.0-dev,
               libgtk2.0-dev,
               libmpd-dev,
               libsoup2.4-dev,
               libsqlite3-dev,
               pkgconf,
               valac,
               zlib1g-dev
Build-Depends-Indep: yelp-tools
Rules-Requires-Root: no
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/mpd-team/gmpc.git
Vcs-Browser: https://salsa.debian.org/mpd-team/gmpc

Package: gmpc
Architecture: any
Depends: gmpc-data (= ${source:Version}), ${misc:Depends}, ${shlibs:Depends}
Suggests: gmpc-plugins, mpd
Enhances: mpd
Provides: mpd-client
Description: GNOME Music Player Client (graphical interface to MPD)
 A graphical client for Music Player Daemon. Features include:
  * Support for loading/saving playlists
  * File Browser
  * Browser based on ID3 information
  * Search
  * Current playlist viewer with search
  * ID3 Information
  * Cover art (via plugins)

Package: gmpc-dev
Architecture: any
Multi-Arch: same
Depends: libmpd-dev,
         pkgconf,
         ${misc:Depends}
Description: GNOME Music Player Client (plugin development files)
 A graphical client for Music Player Daemon. Features include:
  * Support for loading/saving playlists
  * File Browser
  * Browser based on ID3 information
  * Search
  * Current playlist viewer with search
  * ID3 Information
  * Cover art (via plugins)
 .
 This package contains the necessary header files if you wish to compile
 plugins for gmpc.

Package: gmpc-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: gmpc
Description: GNOME Music Player Client - data files
 A graphical client for Music Player Daemon. Features include:
  * Support for loading/saving playlists
  * File Browser
  * Browser based on ID3 information
  * Search
  * Current playlist viewer with search
  * ID3 Information
  * Cover art (via plugins)
 .
 This package contains architecture-independent data for gmpc.
